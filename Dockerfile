FROM solr:9.7.0

ARG IMAGE_VERSION
ENV IMAGE_VERSION ${IMAGE_VERSION}

COPY ./src/configsets/sunspot /opt/solr/server/solr/configsets/sunspot
