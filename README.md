# Solr for Sunspot

Based on the [official Solr docker image](https://hub.docker.com/_/solr) and adds a Solr "configset" for the [Rails Sunspot gem](https://github.com/sunspot/sunspot).

Example Solr service definition for `docker-compose.yml` file:
```dockerfile
  solr:
    image: registry.gitlab.com/bichinger/public/solr-for-sunspot:9.7.0-1
    ports:
      - '8983:8983'
    volumes:
      - ./local_docker_data/solr-9/:/var/solr/
    entrypoint:
      - docker-entrypoint.sh
      - solr-precreate
      - my_solr_core_to_be_created
      - /opt/solr/server/solr/configsets/sunspot
    environment:
      - SOLR_INCLUDE=/var/solr/custom-solr.in.sh
```
This will create a core named `my_solr_core_to_be_created` if it is not
there already (only then!) and use the `sunspot` configset added within this
image. Note: only one core can be precreated by Solr.

You can use the env var `SOLR_INCLUDE` to supply the path to a configuration -
by default it's `/etc/default/solr.in.sh` inside the container and completely
commented out (good reference of available options).
